var words = ["harvesting cherries...", "milling beans...", "roasting coffee...", "grinding coffee...", "brewing...", "coffee ready!"];
var index = 0;

var x1 = 70
var x2 = -70
var y1 = 100
var y2 = 100


function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(250, 190, 100);
  translate(windowWidth / 2, windowHeight / 2);


  fill(51, 26, 0);
  textSize(32);
  text(words[index], 0, -250);
  //the cup

  noFill();
  stroke(51, 26, 0);
  quad(-70, 100, 70, 100, 100,-100, -100,-100);

  //coffee filling up
  noStroke();
  fill(51, 26, 0);
  quad(-70, 100, 70, 100, x1, y1, x2, y2);

  if(x1 >= 70 && x2 <= -70 && y1 >= -100 && y2 >= -100) {
    x1 = x1 + 0.023
    x2 = x2 - 0.023
    y1 = y1 - 0.15
    y2 = y2 - 0.15
  } else if ( y1 <= -100 && y2 <= -100) {
  	x1 = 70
    x2 = -70
    y1 = 100
    y2 = 100
  }

    //the lid
  fill(255);
  noStroke();
  beginShape();
  vertex(-120, -99);
  vertex(-115, -135);
  vertex(-100, -135);
  vertex(-98, -165);
  vertex(98, -165);
  vertex(100, -135);
  vertex(115, -135);
  vertex(120, -99);
  endShape();

  fill(51, 26, 0);
  textSize(32);
  text(words[index], 0, -250);

}
//text function - not optimal though..
function mousePressed() {
  index = index + 1
  if (index == 6){
    index = 0
  }
}
