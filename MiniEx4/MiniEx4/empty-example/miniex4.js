var input1;
var input2;
var input3;
var input4;
var input5;
var input6;
var mood1;
var mood2;
var mood3;
var mood4;
var mood5;
var mood6;
var button1;
var button2;
var button3;
var button4;
var button5;
var button6;
var pic1;
var pic2;
var pic3;
var pic4;
var pic5;
var pic6;

function setup(){
  createCanvas(1200, 700);
  background(250, 0, 150);

  // Button 1
  button1 = createButton("CHEESE");
  button1.position(20, 382);
  button1.mousePressed(showPic1);
  // Button 2
  button2 = createButton("CHEESE");
  button2.position(420, 382);
  button2.mousePressed(showPic2);
  // Button 3
  button3 = createButton("CHEESE");
  button3.position(820, 382);
  button3.mousePressed(showPic3);
  // Button 4
  button4 = createButton("CHEESE");
  button4.position(20, 682);
  button4.mousePressed(showPic4);
  // Button 5
  button5 = createButton("CHEESE");
  button5.position(420, 682);
  button5.mousePressed(showPic5);
  // Button 6
  button6 = createButton("CHEESE");
  button6.position(820, 682);
  button6.mousePressed(showPic6);


  // Picture 1
  pic1 = createCapture(VIDEO);
  pic1.hide();
  pic1.position();
  pic1.size(20, 20);
  // Picture 2
  pic2 = createCapture(VIDEO);
  pic2.hide();
  pic2.position();
  pic2.size(20, 20);
  // Picture 3
  pic3 = createCapture(VIDEO);
  pic3.hide();
  pic3.position();
  pic3.size(20, 20);
  // Picture 4
  pic4 = createCapture(VIDEO);
  pic4.hide();
  pic4.position();
  pic4.size(20, 20);
  // Picture 5
  pic5 = createCapture(VIDEO);
  pic5.hide();
  pic5.position();
  pic5.size(20, 20);
  // Picture 6
  pic6 = createCapture(VIDEO);
  pic6.hide();
  pic6.position();
  pic6.size(20, 20);


  input1 = createInput('Name your expression');
  input1.position(20, 400);
  input1.changed(updateText);
  mood1 = createP('Expression');
  mood1.position(20, 140);
  mood1.style('color','#ffffff')
  input2 = createInput('Name your expression');
  input2.position(420, 400);
  input2.changed(updateText);
  mood2 = createP('Expression');
  mood2.position(420, 140);
  mood2.style('color','#ffffff')
  input3 = createInput('Name your expression');
  input3.position(820, 400);
  input3.changed(updateText);
  mood3 = createP('Expression');
  mood3.position(820, 140);
  mood3.style('color','#ffffff')
  input4 = createInput('Name your expression');
  input4.position(20, 700);
  input4.changed(updateText);
  mood4 = createP('Expression');
  mood4.position(20, 440);
  mood4.style('color','#ffffff')
  input5 = createInput('Name your expression');
  input5.position(420, 700);
  input5.changed(updateText);
  mood5 = createP('Expression');
  mood5.position(420, 440);
  mood5.style('color','#ffffff')
  input6 = createInput('Name your expression');
  input6.position(820, 700);
  input6.changed(updateText);
  mood6 = createP('Expression');
  mood6.position(820, 440);
  mood6.style('color','#ffffff')
}
function updateText(){
  mood1.html(input1.value());
  mood2.html(input2.value());
  mood3.html(input3.value());
  mood4.html(input4.value());
  mood5.html(input5.value());
  mood6.html(input6.value());
}

function showPic1(){
  image(pic1, 20, 100, 300, 200 * pic1.height / pic1.width);
}

function showPic2(){
  image(pic2, 420, 100, 300, 200 * pic2.height / pic2.width);
}

function showPic3(){
  image(pic3, 820, 100, 300, 200 * pic3.height / pic3.width);
}

function showPic4(){
  image(pic4, 20, 400, 300, 200 * pic4.height / pic4.width);
}

function showPic5(){
  image(pic5, 420, 400, 300, 200 * pic5.height / pic5.width);
}

function showPic6(){
  image(pic6, 820, 400, 300, 200 * pic6.height / pic6.width);
}
