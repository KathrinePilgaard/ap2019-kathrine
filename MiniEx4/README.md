# Mini Exercise no.4

![Screenshot](MiniEx4/program1.png)
![Screenshot](MiniEx4/program2.png)

Run me: https://glcdn.githack.com/KathrinePilgaard/ap2019-kathrine/raw/master/MiniEx4/MiniEx4/empty-example/index.html

## README

My program is pretty much about capturing the now - or at least what the user perceives as “the now”.
The program technically consists of three elements. The syntax for capture, but what the computer “sees” is hidden until you click the “CHEESE” button. Afterward the program, via an input bar, allows you to name the expression of your face. The mentioned elements are what my code for the program is based upon. It took some time to figure out how things would interact correctly with each other, and I had to switch out “text” with “createP” to make the program do as I commanded it to.

As mentioned, I see this program as capturing the now, but also at the same time you’re under surveillance (of the computer at least). I look at the program and think of the expressions added to facebook’s former “like”-button. There is, when I look at it, a similarity here, though it might not be obvious, as they have nothing to do with each other. In the program you’re able to “take a picture” though you’re unable to see your face before taking the picture, as there’s no live footage Afterward you’re as mentioned able to name you expression – it has not been picked out by anyone before hand, and e.g. you smiling might not be what you’re feeling, and it is up to you to name what the expression is to you. There’s a freedom here, but you’re “only” able to capture six expressions at a time. No more. If you capture less, it seems like you haven’t “finished” the program. At the same time, you’re able to retake/refreeze the program as many times as you want, and change the description of the expression as well, which again has some freedom to it, but might also in some way be demanding – I mean changing the expression in the “photo” also makes you feel that you have to describe the right expression.

In regard to this weeks reading, the process of coding this program, with the use of the computer’s webcam, has caused me to cross my own boundaries. When reading about the data capturing happening on almost every visited website, I really didn’t like to allow my webcam to be used in the program, as I and you aren’t able to see the live footage, as you e.g. move your head. The amount of data that is captured and stored about you everyday scares me – that might also be the reason that I have a webcam cover, at least then nobody is able to film me without my knowledge.
I do however understand why we have a need to store as much data about users, possible future costumers and so on, for business and sometimes security reasons, but this still just confirms me in the fact that we live in a surveillance society.  
