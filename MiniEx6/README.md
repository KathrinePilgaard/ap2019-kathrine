# Mini Exercise no. 6

![Screenshot](MiniEx6/game.png)

Link: https://glcdn.githack.com/KathrinePilgaard/ap2019-kathrine/raw/master/MiniEx6/MiniEx6/empty-example/index.html

## README
Let me just start by saying that the program is in no way optimal. I ended up giving up on it, after getting help with just making this to work, as it would in no way run just as it is now.
My intention for the program was to make a flower that should collect water drops and thereby receive point for each water drop catched. For now though, it doesn't really do anything. I tried a lot of different syntaxes, but the slightest change in my code somehow made it all stop working. All you can do at this instance in the program is maybe avoid the drops or try catching them, but nothing happens whatsoever…

The objects used in my code, are “waterdrop” and “flowerpot”. The attributes used for the waterdrop was the x position, y position, speed, height, and width. I the assigned the different attributes to the different methods. The methods for the waterdrop were only “move” and “show”.
For the flowerpot I introduced x position, y position and speed. These where then used to enable the object to “moveLeft”, “moveRight” and “show”.

The attributes and methods used, are I believe, based on the assigned readings/videos, some of the most common characteristics of object oriented programming. First you figure out what sort of object you want to work with, and then think about what it should be able to do. Based on these thoughts you should be able to figure out which attributes the object should have. By defining the attibutes, which are most commonly some variable, that enable you to contruct methods – what the object is able to do, when you have put data into the object. The most common methods, from my short acquaintance with object oriented programming, are “show” and “move”, or if it is supposed to be able to different sorts of things, then you can name the action, e.g. “run”, “jump”, “slide” (mostly referring to actions in games).
