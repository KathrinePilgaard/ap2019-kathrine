var flowerpot;
var waterdrops = [];

var waterdropAnimation;
var flowerpotAnimation;

//Loading the images used
function preload(){
  waterdropAnimation = loadAnimation("assets/bubbly0001.png");
  flowerpotAnimation = loadAnimation("assets/asterisk_explode0001.png");
}

function setup() {
  createCanvas(700, 500);

  for (var i = 0; i < 3; i++) {
    waterdrops[i] = new Waterdrop(floor(random(0, 600)), 0, floor(random(1, 4)), floor(random(50, 100), floor(random(50, 100))));
  }

  flowerpot = new Flowerpot(300, 420, 7);
}


function draw() {
  background(0);
//Making the raindrops visible
  for (let i = 0; i< waterdrops.length; i++){
      waterdrops[i].move();
      waterdrops[i].show();


  }

//Functions of the flower
  if(keyIsDown(LEFT_ARROW)){
    flowerpot.moveLeft();
  }
  if(keyIsDown(RIGHT_ARROW)){
    flowerpot.moveRight();
  }
  flowerpot.show();
}

//Waterdrop Class
class Waterdrop {
  constructor(xpos, ypos, speed, h, w){
    this.x = xpos;
    this.y = ypos;
    this.speed = speed;
    this.h = h;
    this.w = w;
  }

  move(){
    this.y += this.speed;
    if (this.y > height+1){
      this.y = 0;
    }
  }

  show(){
    animation(waterdropAnimation, this.x, this.y);
  }
}
//Flower Class
class Flowerpot {
  constructor(xpos, ypos, speed){
    this.x = xpos;
    this.y = ypos;
    this.speed = speed;
  }

  moveLeft(){
    this.x -= this.speed;
  }

  moveRight(){
    this.x += this.speed;
  }

  show(){
    animation(flowerpotAnimation, this.x, this.y);
  }
}
