var size = 20;

function setup() {
  createCanvas(500, 700);
  background(random(255), random(255), 0);
}

function draw() {
  //Frame rate set as otherwise it is uncomfortable to watch
  frameRate(5);
  //
  for(var size = 0; size < 100; size = size + 20){
    fill(random(150, 250), random(100, 250), random(100, 250));
    if(random(2) < 1){
      rect(random(0, 500), random(0, 700), size, size);
      size = size - 1;
    } else {
      size = size + 1;
    }
  }
}
