# Mini Exercise 7
![Screenshot](MiniEx7/miniex7.png)

Link: https://glcdn.githack.com/KathrinePilgaard/ap2019-kathrine/raw/master/MiniEx7/MiniEx7/empty-example/index.html

## README


<b>•  What are the rules in your generative program and describe how your program performs over time. What have been generated beyond just the end product?</b>

The rules for my program are:
-	If rect is drawn, decrease size(variable) by 1
-	If rect not drawn increase size(variable) by 1
-	Change colour for each shape drawn

At first you’re met by a blank canvas, which quickly starts to fill up with rectangles in different sizes and colours. This keeps generating until you close the window, as there is no end to the loop in which I have put the syntaxes and conditional statements to perform the task. At the “end” which is never, your have some sort of “art” pieces if you can call it that  - this could, if you take a screenshot, be a wall paper for your phone, or whatever else you can imagine using it for.

<b>•  What's the role of rules and processes in your work?</b>

The role of the rules and processes in my program, is that they are dependant on each other, and then again not. The what leads to an action being performed, is that the conditions creating the for loop is being met, and then for loop at the same time prevents the “size” of the rect from getting to big as it otherwise would.

The rules mentioned is then found the in conditional statement within the for loop, keeping the rect’s size ever-changing and the use of random in the fill syntax makes sure that the rects’ colour are changing for each new rect as well.


<b>•  What's generativity and automatism? How does this mini-exericse help you to understand what might be generativity and automatism? (see the above - objective 3 and the assigned readings) </b>

Generativity is, I believe, the fact that something keeps generating and evolving – at least under a specific set of circumstances, if the circumstance is no longer met, than the generativity will stop. Automation is something like it, but different. The conditions of automation I understand are more specific in a way, not as “random” as generativity can be. Something that is beforehand set to perform a certain task, in a certain way, where as generativity is not as “square”.

I think my program has helped me understand generativity better in the sense that you have to make certain conditions for it to “thrive” on its own. Only few very simple statements, and then it just keeps generating, I thought about implanting a statement that would course it to stop, but then I thought it might not be seen as, as generative as I would like it to be seen.
