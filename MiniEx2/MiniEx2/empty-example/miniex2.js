function setup() {
  createCanvas(700, 500);
  background(255);
  frameRate(8);

}

function draw() {
//vinyl player
  //background
  noStroke();
  fill(random(255), random(255), random(255));
  rect(0, 0, 350, 500)
  //box beneath
  fill(255);
  stroke(0);
  quad(53, 248, 180, 202, 275, 245, 153, 298);
  quad(53, 248, 153, 298, 153, 323, 53, 273);
  quad(153, 298, 275, 245, 275, 270, 153, 323);

  //vinyl plate
  fill(0);
  ellipse(160, 250, 149, 58);

  //tip
  fill(0, 0, 255)
  noStroke();
  rect(160, 235, 3, 9);

  //strange thingie
  stroke(0);
	rect(245, 238, 10, 12);

  fill(180);
  triangle(172, 253, 178, 253, 175, 270);

  fill(0,255,0);
  quad(245, 242, 245, 247, 172, 255, 172, 250);


//pimple emoji
  //emoji base
  fill(255, 255, 0);
  noStroke();
	ellipse(525, 250, 250, 250);

  //left eye
  fill(255);
  ellipse(480, 235, 60, 60);
  fill(0);
  ellipse(490, 225, 20, 20)

  //right eye
  fill(255);
  ellipse(570, 235, 60, 60);
  fill(0);
  ellipse(560, 225, 20, 20);

  //pimple
  fill(255, 150, 102, 50);
  ellipse(525, 190, 50);

  fill(255, 153, 102, 200);
  ellipse(525, 190, 30)

  fill(255, 0, 0, 150);
  ellipse(527, 187, 15);

  fill(255, random(255), 204);
  ellipse(527, 185, 6);


  //mouth
  fill(0);
  ellipse(525, 300, 10, 15);

}
