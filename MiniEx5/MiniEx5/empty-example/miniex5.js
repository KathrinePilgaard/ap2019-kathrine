var words = ["brew yourself a nice cuppa coffee", "harvesting cherries...", "milling beans...", "roasting coffee...", "grinding coffee...", "brewing...", "coffee ready!"];
var index = 0;

var x1 = 70;
var x2 = -70;
var y1 = 100;
var y2 = 100;
var button;

function setup() {
  createCanvas(displayWidth, displayHeight);
  background(250, 190, 100);
  translate(displayWidth/2, displayHeight/2);
  button = createButton ('fill me up');
  button.position(displayWidth/2, displayHeight/2 + 110);
  button.mousePressed(fillCup);

  //the cup
  noFill();
  stroke(51, 26, 0);
  quad(-70, 100, 70, 100, 100,-100, -100,-100);
  //the lid
  fill(255);
  noStroke();
  beginShape();
  vertex(-120, -99);
  vertex(-115, -135);
  vertex(-100, -135);
  vertex(-98, -165);
  vertex(98, -165);
  vertex(100, -135);
  vertex(115, -135);
  vertex(120, -99);
  endShape();

  fill(51, 26, 0);
  textSize(32);
  text(words[index], 0, -250);

}

function fillCup(){

  fill(51, 26, 0);
  quad(-70, 100, 70, 100, x1, y1, x2, y2);
  x1 = x1 + 0.3;
  x2 = x2 - 0.3;
  y1 = y1 - 2;
  y2 = y2 - 2;
  if (y1 <= -100 && y2 <= -100){
  	x1 = 70
    x2 = -70
    y1 = 100
    y2 = 100

  }
}

// missing the change of array bit...
