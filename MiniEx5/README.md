# Mini Exercise no. 5

![Screenshot](MiniEx5/brewcoffee.png)
![Screenshot](MiniEx5/brewcoffee2.png)

Link: https://glcdn.githack.com/KathrinePilgaard/ap2019-kathrine/raw/master/MiniEx5/MiniEx5/empty-example/index.html

## README

This week’s program is a remodelling of the program uploaded to mini_ex3. The program I previously produced, was meant to be a throbber with a text that should change as the throbber “made further progress”.
This new design of the program is still a throbber, but a different kind of buffer. Some people might find it annoying that the program demands them to take action in order to (supposedly) get to the next page, or as the program depicts get your coffee.

To change the program, I basically rewrote the basics of the old program. Then I ended up struggling with a mousePressed syntax that was just really stubborn. To get around this, I took some wise advice from a friend, and introduced a button in the program – which at the same time made the program make more sense, as I hadn’t thought of a certain place you should press in order to get the cup to fill.

The new function was something that I had already reflected upon in my first edition of the program, but didn’t implement, as I weren’t able to produce this at the given time of the first program. Making this change though prevents me from using the array of words from the former program in the way they were previously used. Actually at this given moment they are not being used at all, but I changed the first sentence in the array, then what you’re doing at least makes some sense. Preferably the text should change throughout the program, but I still haven’t been able to figure out which syntaxs and commands to use to get the program where I want it to be.


The concept of the new work is a bit different from the previous in the way, that the user (as mentioned) is demanded to keep pressing the button over and over again for the next page to “load” – not that it actually makes a difference to the loading and processing of the next page the user will be met by. This is more of a constraint that anything else, not allowing the user directly access to the next page. It is still a throbber, and if it worked as intended the user would in some way be able to understand what is going on the “inside” of the running program.

What this program does, I think, is provoking the user to take action, and it might be and expression of rebellion against the loss of time and the feeling of inactiveness we experience when waiting for a program to load.
I knew from the moment we were assigned this task that it was the throbber program that I wanted to redo, as I was in no way satisfied with how it turned out the first time around, and again now I’m displeased with the turnout of the changes. I made the changes to program because I(as mentioned) previously had thought about this way of constructing a throbber, and I also got some good/encouraging feedback on the thought of what I have now done to “load” the program, so I saw this as an opportunity to get a do over.
