# Mini Exercise no. 8
![Screenshot](MiniEx8/miniex8.png)

Link: https://glcdn.githack.com/KathrinePilgaard/ap2019-kathrine/raw/master/MiniEx8/MiniEx8/empty-example/index.html


## README
This program is the outcome of a collaboration between Lise Emilia Duelund and Kathrine Pilgaard.

The program is about exchanging/replacing words in a very simple sentence. The original sentence goes “Ain’t no sunshine when she’s gone” which is a rather well-known sentence from a song. We removed some of the words and created a JSON with words that could fill in the spaces left blank in the sentence. The words to fill in are chosen randomly within their categories. The words change when you press the screen, as we did not want to implement a button for aesthetic reasons.

The collaborative process was good I think. When we finally(not that it took long) settled upon a certain way of approaching the task, we quickly formed an idea that both of us agreed on and was satisfied with. The Teletype program was a great help, especially in creating the JSON file, as it was nice to be able to write different categories at the same time without having to wait for one another. Otherwise we build the program from scratch and had a bit help here and there, but at the end we were satisfied with how it turned out.

Trying to contextualize the program, I first of all see it as a sort of critical art piece, as some of the sentences you end up with are critical upon certain aspects of society – different sorts of taboos comes forward when running the program multiple times; other times you just end up with a funny sentence that does not seem to have a deeper meaning or touches you as deeply.

Connecting the “e-lit”-work to “Vocable Code”, I would not look at the code by it self and have a greater idea of what it is going to do. The code is not, as other codes can be, poetry in it self – it is first when executed it makes sense in the bigger picture. We did though when writing the code think about what we named the variable, as we made the program about <i>replacing</i> words with others, which is why it the word “replace” appears in various forms throughout the program; not that computer has any idea of what the meaning of the variables are, but we do.   
