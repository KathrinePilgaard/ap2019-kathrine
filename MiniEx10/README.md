# Mini Exercise no. 10

## Individual work
![Screenshot](MiniEx10/miniex4flowchart.png)

MiniEx4 folder: https://gitlab.com/KathrinePilgaard/ap2019-kathrine/tree/master/MiniEx4

For the flowchart I had to make on my own, I went back to the “Capture all”-mini exercise, which was mini exercise no. 4. I chose this specific mini exercise to do flowchart on, due to both the theme of Capture All, but also because it one of the more complex programs I’ve done, which actually works. I spend a lot of time on this mini exercise trying to make it work as intended and it pretty much does.
The flowchart I’ve made does perhaps not display the complexity very well, but more that the user has a lot of influence on the program and how it ends up. The specifics of the flowchart I chose not to make technical, as I find it better to make a flowchart that will be readable to people who does not necessary have knowledge of the programming world, thereby making what the program is about understandable to everyone.
I think how I’ve displayed the program in the flowchart presents the user influence very well, but also that the program demands the user to do a lot for it to work as intended.

The difficulty in drawing this flowchart was actually that I had already made the program, and that somehow made it – not more difficult – just as difficult to draw it, as it was to draw flowchart on non-existing final project. I think the difficulty here was because I had to figure out what I wanted to present, if it was all the work that laid before the user being able to press the button – then including “create 6 hidden cameras”, “create a button for each camera” etc. – or just the program as it is seen by the user(which was what I chose to do).

## Group Work - Two flowcharts
### TANSA: THE ARTIFICIAL AI BOT
![Screenshot](MiniEx10/MINIEX10TANSA.png)

We have the idea of making our own artificial AI bot called TANSA. We want to write the answers of the AI bot as text lines in a JSON file together with sound files of us reading the lines aloud. All in the group will use their voices to record the lines. This is to include both male and female voices and thereby to contribute to the equality debate in digital culture. This is also to break with the manipulation of how consumers perceive information given from different genders. Male voices have a tendency to carry more authority. Female voice tends to be more persuasive.

We are not skilled enough to make a real interaction with AI. So, one of the main challenges of this program is that we want to imitate a mic input interaction with our TANSA bot giving the illusion of the bot answering your question. To do that, we might have to make a conditional statement saying that the button has to be pressed for at least 2 seconds before the bot fetches an answer. While pressing the mic button a throbber will indicate that the button is pressed. We are going to use the boolean statement of mousePressed meaning that the mic button has to be held down and then released before the JSON data is called. Another option for constraining the JSON data access is to let the mic input decide it. So, if the program captures a specific volume level the JSON data is accessed and fetched.

We have called our AI bot ‘TANSA’ which is an acronym for There Are No Stupid Answers. We want to make a critical statement with this program, by somewhat ridiculing the way we tend to lay our trust in machines. So, we basically want people to reconsider their use of machines and make them engage with machines with a more critical sense, not taking everything for gospel truth.

### The Sound of Automation
![Screenshot](MiniEx10/miniex10automation.png)

When is art really art? Does it have to be created by a human or can a machine generate art? We want to test this with a program that generates a piece of digital art combined with music. We as creators of the program will set some rules for the program to execute and thereby give the machine the possibility to create a piece of art on its own. This means that there are still humans behind it, making us the overall authors.

We want to create a program that generates art inspired by Mathias’ mini_ex7 where he created a generative art piece consisting of ellipses and frames of other shapes appearing when the ellipses would reach specific points. We have used similar design elements by making ellipses appear from the middle of the canvas, moving randomly and creating a randomly generated pattern. But we have redesigned the behavior of it crossing the borders. When it reaches either the bottom, top, right or left border a sound of a chord will play and a geometric shape will appear, continuously growing in size.

One of the challenges with this idea is that we are unsure of how to implement the sound output when the ellipses reach the borders.



<b> Individual </b>

•	How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?

Different from the flowchart that I made for an old program, the difficulty in drawing this flowchart was that, first of all, we were a lot of people who had to agree upon what our future final project should be about, look like and how it should work. When that had for the most part been agreed upon, we then had to specify exactly what we were thinking the program had to do in each particular step – this was where we found that maybe we did not all have the same idea in our heads, but we worked it out.
Also making a flowchart before the actual program also made us, as a group, think about what we would be able to program, and not setting the bar to high(hopefully). We did and still do not want to set the bar to high, but just high enough for us to be able to wrap our head around what we can possibly accomplish.

•	If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)

Algorithms are displayed/explained amongst other things as problem solvers, a way to sort things, and they do these things, is by following a set of rules, which has been predetermined by programmers/creators of the algorithms. These rules can be, conditional statements, but also read as a recipe for how to do thing, and what to do if certain needs are met, and if certain needs are not met.
The same way algorithms are described as recipes; flowcharts can be seen as recipes as well. Where algorithms are implemented in the running program; flowcharts are “recipes” for how to make the program, what the program shall include, what choices can be made and so on. The flowchart is a visual representation for what needs to be included. In the flowchart there can be found traces of an algorithms, or multiple algorithms, depending on how thorough and how technical the flowchart is.
