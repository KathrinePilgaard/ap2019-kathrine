function setup() {
  createCanvas(700, 500);
	background(255, 165, 0);

}

let value = 250;
function draw() {
  noStroke();
	ellipse(438, 250, 50, 54);

  fill(255, 165, 0);
  ellipse(438, 250, 32, 36);

	rectMode(CENTER);
	fill(50, value, 150);
	rect(350, 250, 150, 100, 0, 0, 15, 15);

}

function mouseMoved() {
  value = value + 1 ;
  if (value > 255) {
    value = 0;
  }
}
